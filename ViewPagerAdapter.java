package vampire.com.myapplication;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created My Application by *Vampire* on 16/9/1.
 */
public class ViewPagerAdapter extends PagerAdapter{
    private static final String TAG = "Vampire_ViewPagerAdapter";
    private ArrayList<Integer> pic ;
    private Context mContext;

    public void setPic(ArrayList<Integer> pic) {
        this.pic = pic;
        notifyDataSetChanged();
    }

    public ViewPagerAdapter(Context mContext) {
        this.mContext = mContext;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.viewpager_recommend,container,false);
        ImageView imageView = (ImageView) view.findViewById(R.id.iv_recommend);
        imageView.setImageResource(pic.get(position%pic.size()));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(container);
    }
}
