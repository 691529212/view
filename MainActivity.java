package vampire.com.myapplication;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.view_pager_recommend);

        ViewPagerAdapter adapter =new ViewPagerAdapter(this);
        ArrayList<Integer> integers =new ArrayList<>();
        integers.add(R.mipmap.first);
        integers.add(R.mipmap.second);
        integers.add(R.mipmap.tired);
        integers.add(R.mipmap.fourth);
        adapter.setPic(integers);
        viewPager.setAdapter(adapter);

    AutoTurnPager();

    }
    private void AutoTurnPager() {
        viewPager.setCurrentItem(9998,false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    sleep();
                    mHandler.sendEmptyMessage(viewPager.getCurrentItem());
                }
            }
        }).start();
    }
    private void sleep(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private Handler mHandler =new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            int currentItem =msg.what;
            viewPager.setCurrentItem(currentItem+1);
            return false;
        }
    });
}
